package ua.khpi.oop.lukianchykova09;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import ua.khpi.oop.lukianchykova07.Composition;
import ua.khpi.oop.lukianchykova07.Rating;

public class Main {

	public static void main(String[] args) {
		MyContainer<Composition> audioLibrary = new MyContainer<Composition>();
		int num = 1;
		String text = "Позови меня с собой,\nЯ приду сквозь злые ночи,\nЯ отправлюсь за тобой,\nЧтобы путь мне не пророчил,\nЯ приду туда, где ты\nНарисуешь в небе солнце,\nГде разбитые мечты\nОбретают снова силу высоты";
		Rating[] rate = new Rating[3];
		rate[0] = new Rating("Прослушали", 1940400);
		rate[1] = new Rating("Продажи", 10000);
		rate[2] = new Rating("Радио", 2500);
		Composition composition = new Composition(num++, "Позови меня с собой", "Поп-музыка", "Алла Пугачёва", text, 21, 8, 1995, "4:17", ".mp3", rate);
		audioLibrary.add(composition);
		
		text = null;
		rate[0] = new Rating("Прослушали", 1000000);
		rate[1] = new Rating("Продажи", 4000);
		rate[2] = new Rating("Радио", 500);
		composition = new Composition(num++, "Каждый кто делал тебе больно", "Авторская песня", "Леро4ка", text, 9, 7, 2021, "3:33", ".mp3", rate);
		audioLibrary.add(composition);
		
		text = "На ветру белое платье,\nНа тебе проклятье,\nМне нужны объятья, поцелуи,\nЧасть тебя.\nРазлетелся пепел в поле,\nЯ бегу по полю,\nС тобою небеса.";
		rate[0] = new Rating("Прослушали", 774000);
		rate[1] = new Rating("Продажи", 1000);
		rate[2] = new Rating("Радио", 200);
		composition = new Composition(num++, "Ведьмы", "Авторская песня", "Bavkon", text, 1, 12, 2021, "1:34", ".mp3", rate);
		audioLibrary.add(composition);
		Menu(audioLibrary);
	}
	
	/**
	 * Menu realisation
	 * @param 
	 * */
	public static void Menu(MyContainer<Composition> audioLibrary) {
		boolean endprog = false;
		Scanner inInt = new Scanner(System.in);
		Scanner inStr = new Scanner(System.in);
		int menu;
		
		while(!endprog) {
			System.out.println("1. Show all compositions");
			System.out.println("2. Add composition");
			System.out.println("3. Delete composition");
			System.out.println("4. Clear list");
			System.out.println("5. Check for emptiness");
			System.out.println("6. Serialize data");
			System.out.println("7. Deserialize data");
			System.out.println("0. Exit");
			System.out.print("Enter option: ");
			try {
				menu =  inInt.nextInt();
			}
			catch(java.util.InputMismatchException e) {
				System.out.println("Error! Ошибка ввода.");
				endprog = true;
				menu = 0;
			}
			System.out.println();
			
			switch(menu) {
			case 1:
				if(audioLibrary.getSize() > 0) {
					for(var element : audioLibrary) {
						element.print();
					}
				}
				else {
					System.out.println("The list is empty!\n");
				}
				break;
			case 2:
				if(addComposition(audioLibrary, inInt, inStr) == -1) {
					break;
				}
				break;
			case 3:
				System.out.println("Enter number of composition to delete: ");
				int delete = inInt.nextInt();
				boolean isExist = false;
				if(audioLibrary.getSize() > 0) {
					for(Composition element : audioLibrary) {
						if(element.getNum() == delete) {
							isExist = true;
						}
						if(element.getNum() > delete) {
							element.setNum(element.getNum() - 1);
						}
					}
					if(isExist) {
						if(audioLibrary.delete(delete - 1)) {
							System.out.println("Composition was deleted successfully.");
						} else
							System.out.println("Error! Wrong number.");
					} else
						System.out.println("Error! Wrong number.");
				}
				break;
			case 4:
				audioLibrary.clear();
				System.out.println("The list is empty now.\n");
				break;
			case 5:
				if(audioLibrary.isEmpty())
					System.out.println("The list is empty.\n");
				else
					System.out.println("The list is not empty.");
				break;
			case 6:
				String filenameSerialization;
				String filenameXML;
				int menuSerialization;
				
				System.out.println("1. Serialization");
				System.out.println("2. XML serialization");
				System.out.println("0. Exit serialization");
				try 
				{
					menuSerialization =  inInt.nextInt();
				}
				catch(java.util.InputMismatchException e) 
				{
					System.out.println("Error! Ошибка ввода.");
					menuSerialization = 0;
				}
				switch(menuSerialization) 
				{
				case 1:
					System.out.println("\nEnter file name: ");
					filenameSerialization = inStr.nextLine();
					if (filenameSerialization.indexOf(".ser") == -1) {
						filenameSerialization += ".ser";
					}
					try(ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream (filenameSerialization)))){
						oos.writeObject(audioLibrary);
						System.out.println("Serialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 2:
					System.out.print("Enter XML filename: ");
					filenameXML = inStr.nextLine();
					if (filenameXML.indexOf(".xml") == -1)
						filenameXML += ".xml";
					try(XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream (filenameXML)))){
						encoder.writeObject(audioLibrary);
						System.out.println("Serialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 0:
					break;
				default:
					System.out.println("Error! Wrong num in menu.");
					break;
				}
				break;
			case 7:
				String filenameDeserialization;
				int menuDeserialization;
				
				System.out.println("1. Deserialization");
				System.out.println("2. XML deserialization");
				System.out.println("0. Exit deserialization");
				try 
				{
					menuDeserialization =  inInt.nextInt();
				}
				catch(java.util.InputMismatchException e) 
				{
					System.out.println("Error! Ошибка ввода.");
					menuDeserialization = 0;
				}
				switch(menuDeserialization) 
				{
				case 1:
					System.out.println("\nEnter file name: ");
					filenameDeserialization = inStr.nextLine();
					if (filenameDeserialization.indexOf(".ser") == -1) {
						filenameDeserialization += ".ser";
					}
					try(ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
						audioLibrary.clear();
						audioLibrary=(MyContainer<Composition>) ois.readObject();
						System.out.println("Deserialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 2:
					System.out.print("Enter XML filename: ");
					filenameDeserialization = inStr.nextLine();
					if (filenameDeserialization.indexOf(".xml") == -1)
						filenameDeserialization += ".xml";
					try(XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
						audioLibrary.clear();
						audioLibrary =  (MyContainer<Composition>) decoder.readObject();
						System.out.println("Deserialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 0:
					break;
				default:
					System.out.println("Error! Wrong num in menu.");
					break;
				}
				break;
			case 0:
				endprog = true;
				inInt.close();
				inStr.close();
				break;
			default:
				System.out.println("Error! Wrong num.");
				break;
			}
		}
	}
	
	/**
	 * Add new composition
	 * @param 
	 * */
	public static int addComposition(MyContainer<Composition> audioLibrary, Scanner inInt, Scanner inStr) {
		int num = audioLibrary.getSize() + 1;
		Rating[] rate = new Rating[3];
		String text = null;
		String name;
		String performer;
		String genre;
		int day;
		int month;
		int year;
		String duration;
		String format;
		
		System.out.println("Введите название: ");
		try {
			name = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите исполнителя: ");
		try {
			performer = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите жанр: ");
		try {
			genre = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Есть ли текст? [Да(1)/Нет(0)]: ");
		int c = inInt.nextInt();
		if (c == 1) {
			System.out.println("Введите текст: ");
			try {
				text = inStr.nextLine();
			}catch(java.util.InputMismatchException e) {
				System.out.println("Error! Incorect input!");
				return -1;
			}
		}
		
		System.out.println("Введите день выпуска: ");
		try {
			day = inInt.nextInt();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите месяц выпуска: ");
		try {
			month = inInt.nextInt();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите год выпуска: ");
		try {
			year = inInt.nextInt();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите длительность: ");
		try {
			duration = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите формат: ");
		try {
			format = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите кол-во прослушиваний: ");
		try {
			rate[0] = new Rating("Прослушали", inInt.nextInt());
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		System.out.println("Введите кол-во продаж: ");
		try {
			rate[1] = new Rating("Продажи", inInt.nextInt());
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		System.out.println("Введите кол-во прокруток по радио: ");
		try {
			rate[2] = new Rating("Радио", inInt.nextInt());
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		Composition composition = new Composition(num, name, genre, performer, text, day, month, year, duration, format, rate);
		audioLibrary.add(composition);
		return 1;
	}
	
}
