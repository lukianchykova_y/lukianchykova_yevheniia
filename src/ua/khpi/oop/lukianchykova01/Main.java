package ua.khpi.oop.lukianchykova01;

public class Main {
	/**
	 * Counts the number of unpaired digits
	 * @param num number
	 * @return odd result
	 * */
	private static int countOdd(long num) {
		int odd = 0;
		while (num > 0) {
			if (num % 2 != 0) 
				odd++;
			num /= 10;
		}
		return odd;
	}
	/**
	 * Counts the number of paired digits
	 * @param num number
	 * @return even result
	 * */
	private static int countEven(long num) {
		int even = 0;
		while (num > 0) {
			if (num % 2 == 0) 
				even++;
			num /= 10;
		}
		return even;
	}
	/**
	 * Counts the number of ones in binary number
	 * @param num number
	 * @return count result
	 * */
	private static int countOnes(long num) {
		int count;
		for(count = 0; num != 0; count++)
			num &= (num - 1);
 		return count;
	}
	/**
	 * Main function
	 * @param args arguments of function
	 * */
	public static void main(String[] args) {
		/*
		 * Завдання 1: 
		 * 	Обрати тип змінних та встановити за допомогою констант та літералів початкові значення.
		 */
		final int recordNum = 0x32A; // Record book number
		final long phoneNum = 380990841342L; // Phone Number
		final int phoneBin = 0b101010; // 2 last numbers in binary system
		final int phoneOct = 02476; // 4 last numbers in octal system
		final int studNum = 12; // My number
		int task = (studNum - 1) % 26 + 1; 
		char symbol = (char) (65 + task);
		
		/*
		 * Завдання 2:
		 * 	Використовуючи десятковий запис цілочисельного значення кожної змінної 
		 *  знайти і підрахувати кількість парних і непарних цифр.
		 */ 
		int odd = countOdd(recordNum);
		int even = countEven(recordNum);
		odd = countOdd(phoneNum);
		even = countEven(phoneNum);
		odd = countOdd(phoneBin);
		even = countEven(phoneBin);
		odd = countOdd(phoneOct);
		even = countEven(phoneOct);
		odd = countOdd(task);
		even = countEven(task);
		odd = countOdd(symbol);
		even = countEven(symbol);
		System.out.println("Odd: " + odd + "\nEven: " + even);
		
		/*
		 * Завдання 3:
		 * Використовуючи двійковий запис цілочисельного значення кожної змінної підрахувати 
		 * кількість одиниць.
		 */ 
		int ones = 0;
		ones = countOnes(recordNum);
		ones = countOnes(phoneNum);
		ones = countOnes(phoneBin);
		ones = countOnes(phoneOct);
		ones = countOnes(task);
		ones = countOnes(symbol);
		System.out.println("Ones: " + ones);
	}

}

