package ua.khpi.oop.lukianchykova08;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;
import ua.khpi.oop.lukianchykova07.Composition;
import ua.khpi.oop.lukianchykova07.Rating;

public class Main {
	
	/**
	 * Main function
	 * @param args arguments
	 * */
	public static void main(String[] args) {
		AudioLibrary list = new AudioLibrary();
		int num = 1;
		String text = "Позови меня с собой,\nЯ приду сквозь злые ночи,\nЯ отправлюсь за тобой,\nЧтобы путь мне не пророчил,\nЯ приду туда, где ты\nНарисуешь в небе солнце,\nГде разбитые мечты\nОбретают снова силу высоты";
		Composition composition = new Composition(num++, "Позови меня с собой", "Поп-музыка", "Алла Пугачёва", text, 21, 8, 1995, "4:17", ".mp3", list.rate);
		list.rate[0] = new Rating("Прослушали", 1940400);
		list.rate[1] = new Rating("Продажи", 10000);
		list.rate[2] = new Rating("Радио", 2500);
		list.add(composition);
		
		text = null;
		composition = new Composition(num++, "Каждый кто делал тебе больно", "Авторская песня", "Леро4ка", text, 9, 7, 2021, "3:33", ".mp3", list.rate);
		list.rate[0] = new Rating("Прослушали", 1000000);
		list.rate[1] = new Rating("Продажи", 4000);
		list.rate[2] = new Rating("Радио", 500);
		list.add(composition);
			
		text = "На ветру белое платье,\nНа тебе проклятье,\nМне нужны объятья, поцелуи,\nЧасть тебя.\nРазлетелся пепел в поле,\nЯ бегу по полю,\nС тобою небеса.";
		composition = new Composition(num++, "Ведьмы", "Авторская песня", "Bavkon", text, 1, 12, 2021, "1:34", ".mp3", list.rate);		
		list.rate[0] = new Rating("Прослушали", 774000);
		list.rate[1] = new Rating("Продажи", 1000);
		list.rate[2] = new Rating("Радио", 200);
		list.add(composition);
			
		Menu(list, num);
	}

	
	/**
	 * Menu realisation
	 * @param list compositions
	 * @param num number of last composition
	 * */
	public static void Menu(AudioLibrary list, int num) {
		boolean endprog = false;
		Scanner inInt = new Scanner(System.in);
		Scanner inStr = new Scanner(System.in);
		int menu;
		
		while(!endprog)
		{
			System.out.println("1. Вывести все композиции");
			System.out.println("2. Добавить композицию");
			System.out.println("3. Удалить композицию");
			System.out.println("4. Очистить список");
			System.out.println("5. Serialize or Deserialize");
			System.out.println("6. Выход");
			System.out.println("Выберите действие: \n");
			try {
				menu =  inInt.nextInt();
			}
			catch(java.util.InputMismatchException e) {
				System.out.println("Error! Ошибка ввода.");
				endprog = true;
				menu = 7;
			}

			switch(menu) {
			case 1:
				list.printAll();
				break;
			case 2:
				addComposition(list, num++, inInt, inStr);
				break;
			case 3:
				System.out.println("Введите номер композиции, которую хотите удалить: ");
				int delete = inInt.nextInt();
				if(list.remove(delete - 1)) {
					num--;
					System.out.println("Композиция удалена.");
				}
				else {
					System.out.println("Error!");
					menu = 7;
				}
				break;
			case 4:
				list.clear();
				System.out.println("Список очищен.");
				break;
			case 5:
				FolderMenu(inInt, inStr, list);
				break;
			case 6:
				endprog = true;
				inInt.close();
				inStr.close();
				break;
			default:
				System.out.println("Error! Wrong num in menu.");
				break;
			}
		}
	}
	
	/**
	 * Add new composition
	 * @param list compositions
	 * @param num number of last composition
	 * @param inInt for int
	 * @param inStr for string
	 * */
	public static void addComposition(AudioLibrary list, int num, Scanner inInt, Scanner inStr) {
		String text = null;
		System.out.println("Введите название: ");
		String name = inStr.nextLine();
		System.out.println("Введите исполнителя: ");
		String performer = inStr.nextLine();
		System.out.println("Введите жанр: ");
		String genre = inStr.nextLine();
		System.out.println("Есть ли текст? [Да(1)/Нет(0)]: ");
		int c = inInt.nextInt();
		if (c == 1) {
			System.out.println("Введите текст: ");
			text = inStr.nextLine();
		}
		System.out.println("Введите день выпуска: ");
		int day = inInt.nextInt();
		System.out.println("Введите месяц выпуска: ");
		int month = inInt.nextInt();
		System.out.println("Введите год выпуска: ");
		int year = inInt.nextInt();
		System.out.println("Введите длительность: ");
		String duration = inStr.nextLine();
		System.out.println("Введите формат: ");
		String format = inStr.nextLine();
		System.out.println("Введите кол-во прослушиваний: ");
		list.rate[0] = new Rating("Прослушали", inInt.nextInt());
		System.out.println("Введите кол-во продаж: ");
		list.rate[1] = new Rating("Продажи", inInt.nextInt());
		System.out.println("Введите кол-во прокруток по радио: ");
		list.rate[2] = new Rating("Радио", inInt.nextInt());
		
		Composition composition = new Composition(num, name, genre, performer, text, day, month, year, duration, format, list.rate);
		list.add(composition);
	}

	
	/**
	 * Menu for directory change
	 * @param list compositions
	 * @param inInt for int
	 * @param inStr for string
	 * */
	public static void FolderMenu(Scanner inInt, Scanner inStr, AudioLibrary list) {
		boolean endProcess = false;
		boolean folderChoose = true;
		int menu;
		String absolutePath = new File("").getAbsolutePath();
		File folder = new File(absolutePath);
		File[] listFiles = folder.listFiles();
		String currentDir = absolutePath;
		String highestDir = folder.getName();
		int position = 0;
		
		while(!endProcess) {
			System.out.println("\nCurrent path: " + currentDir);
			System.out.println("\nFiles and directories in this path: ");
			for (position = 0; position < listFiles.length; position++) 
				System.out.println(position + 1 + ". " + listFiles[position].toString().substring(currentDir.length() + 1));
			System.out.println();
			System.out.println("Process menu: ");
			System.out.println("1. Work in current directory");
			System.out.println("2. Go up one level folder");
			System.out.println("3. Enter the folder");
			System.out.println("4. End of process");
			System.out.print("Enter option: ");
			menu = inInt.nextInt();
			System.out.println();
			switch(menu) {
			case 1:
				StartWork(inInt, inStr, currentDir, position, listFiles, list);
				endProcess = true;
				break;
			case 2:
				if(folder.getName().equals(highestDir)) {
					System.out.print("This is the highest directory.");
					break;
				}
				currentDir = currentDir.substring(0, currentDir.indexOf(folder.getName())-1);
				folder = new File(currentDir);
				listFiles = folder.listFiles();
				break;
			case 3:
				while(folderChoose) {
					System.out.print("Choose the number of folder: ");
					position = inInt.nextInt();
					if(!listFiles[position - 1].isDirectory() || position < 1 || position > listFiles.length)
						System.out.println("Error. That is not a folder.");
					else {
						currentDir = listFiles[position - 1].toString();
						System.out.println("New current directory: " + currentDir);
						folder = new File(currentDir);
						listFiles = folder.listFiles();
						folderChoose = false;
					}
				}
				break;
			case 4:
				System.out.println("End of process");
				System.out.println();
				endProcess = true;
				break;
			default:
				System.out.println("Error! Wrong num in menu.");
				break;					
			}
		}
	}
	
	/**
	 * Menu for serialization and deserialization
	 * @param list compositions
	 * @param inInt for int
	 * @param inStr for string
	 * @param position current id of directory
	 * @param currentDir current directory
	 * @param listFiles array with files
	 * */
	public static void StartWork(Scanner inInt, Scanner inStr, String currentDir, int position, File[] listFiles, AudioLibrary list) {
		String filename;
		System.out.println("Choose: ");
		System.out.println("1. Serialization");
		System.out.println("2. Deserialization");
		System.out.print("Enter option: ");
		int menu = inInt.nextInt();
		switch(menu) {
		case 1:
			System.out.print("Enter the name of XML file: ");
			filename = inStr.nextLine();
			if (filename.indexOf(".xml") == -1)
				filename += ".xml";
			System.out.println("XML file name: " + filename);
			String absolutePath = currentDir;
			File folder = new File(absolutePath);
			File file = new File(folder,filename);
			try {
				XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(file)));
				encoder.writeObject(list.mas);
				encoder.close();
			} 
			catch (Exception e) {
				System.out.println(e);
				break;
			}
			System.out.println("File was written in this directory: " + absolutePath);
			System.out.println("Serialization was completed.");
			break;
		case 2:
			System.out.print("Enter number of the file: ");
			position = inInt.nextInt();
			absolutePath = currentDir + "\\" + listFiles[position - 1].getName();
			file = new File(absolutePath);
			
			if(listFiles[position - 1].getName().indexOf(".xml") == -1 || listFiles[position - 1].isDirectory()) {
				System.out.println("Error, that's not a .XML file.");
				break;
			}
			try {
				XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(file)));
				list.mas = (Composition[])decoder.readObject();
				decoder.close();
				list.setSize(list.mas.length);
			} 
			catch (Exception e) {
				System.out.println(e);
				break;
			}
			System.out.println("File was read from this directory: " + listFiles[position - 1]);
			System.out.println("Deserialization was completed.");
			break;
		}
		
	}
}


