package ua.khpi.oop.lukianchykova08;

import ua.khpi.oop.lukianchykova07.*;

public class AudioLibrary {
	private int size = 0;
	Composition[] mas = new Composition[3];
	Rating[] rate = new Rating[3];
	
	/**
	 * Getter of size
	 * @return size
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Setter of size
	 * @param size size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	/**
	 * Add new composition
	 * @param composition composition
	 */
	public void add(Composition composition) {
		Composition[] arr = new Composition[size+1];
		for(int i = 0; i < size; i++)
			arr[i] = mas[i];
		size++;
		arr[size-1] = composition;
		mas = arr;
	}
	
	/**
	 * Delete composition
	 * @param num number of composition
	 * @return true if composition removed successfully 
	 */
	public boolean remove(int num) {
		int number = num + 1;
		if(num >= 0 && num < size) {
			Composition[] arr = new Composition[size-1];
			for (int i = 0; i < num; i++) 
				arr[i] = mas[i];
			for (int i = num, j = num + 1; j < size; i++, j++) 
				arr[i] = mas[j];
			size--;
			mas = arr;
			for (int i = num; i < size; i++) 
				mas[i].setNum(number++);;
			return true;
		}
		return false;
	}
	
	/**
	 * Clear container 
	 */
	public void clear() {
		size = 0;
		mas = new Composition[0];
	}
	
	/**
	 * Print comosition
	 * @param num number of composition
	 */
	public void print(int num) { 
		if(num > 0 || num < size) {
			num -= 1;
			mas[num].print();
		}
		else {
			System.out.println("Вы ввели неверный номер.");
		}
	}
	
	/**
	 * Print all compositions 
	 */
	public void printAll() { 
		if (size > 0) {
			for(int i = 0 ; i < mas.length; i++) {
				mas[i].print();
			}
		}
		else {
			System.out.println("В списке нет композиций.");
		}
	}
}
