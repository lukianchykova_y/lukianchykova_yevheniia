package ua.khpi.oop.lukianchykova05;

import java.util.Iterator;

public class Main {
	
	/**
	 * Main function
	 * @param args arguments
	 * */
	public static void main(String[] args) {
		Container container = new Container("If you come to our house, you enter our hall first.","Here you may take off your coat and shoes.","The floor is covered with a fitted carpet, so you will feel comfortable in our house.");
		Iterator<String> iterator = container.iterator();
		Container helpContainer = new Container("If you come to our house, you enter our hall first.", "The floor is covered with a fitted carpet, so you will feel comfortable in our house.", "There is a kitchen, a bathroom, and a living room.");
		
		cycles(container, iterator);
		checkRemove(container, "Here you may take off your coat and shoes.");
		checkadd(container);
		
		System.out.println("Data of second container: " + helpContainer.toString());
		System.out.println("Main container contains string <There is a kitchen, a bathroom, and a living room>:" + container.contains("There is a kitchen, a bathroom, and a living room."));
		System.out.println("Main container contains all: " + container.containsAll(helpContainer));
		
	}
	
	/**
	 * Check iterator in cycles
	 * @param container container
	 * @param iterator iterator
	 * */
	public static void cycles(Container container, Iterator<String> iterator) {
		System.out.println("Cycle <while>");
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.print('\n');
		
		System.out.println("Cycle <for each>");
		Object[] array = container.toArray();
		for(Object arr : array) {
			System.out.println(arr);
		}
		System.out.print('\n');
	}
	
	/**
	 * Check remove method 
	 * @param container container
	 * @param str string to remove
	 * */
	public static void checkRemove(Container container, String str) {
		System.out.println("Removing one item from container");
		System.out.println("Check removing:" + container.remove(str));
		System.out.println(container.toString());
		System.out.println("Size of container:" + container.size());
		System.out.print('\n');
	}
	
	/**
	 * Check add method 
	 * @param container container
	 * */
	public static void checkadd(Container container) {
		System.out.println("Add some new items.");
		container.add("There is a kitchen, a bathroom, and a living room.");
		container.add("There is a big window and small curtains here, so the sunshine fills it, and makes it cozy.");
		container.add("There is a gas cooker, a refrigerator, a cupboard, a sink unit and a big round table here.");
		System.out.println("Data: " + container.toString());
		System.out.println("Size of container:" + container.size());
		System.out.print('\n');
	}
	
}
