package ua.khpi.oop.lukianchykova05;

import java.util.Iterator;

public class Container {
	private String[] stringarray;
	private int size = 0;
	
	/**
	 * Constructor
	 * @param strarr - string array
	 */
	public Container (String... strarr) {
		if (strarr.length != 0) {
			size = strarr.length;
			stringarray = new String [size];
			for (int i = 0; i < size; i++)
				stringarray[i] = strarr[i];
		}
	}
	
	/**
	 * Returns yhe contents of the container as a string
	 * @return sb contents as string
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++)
			sb.append(stringarray[i] + " ");
		return sb.toString();
	}
	
	/**
	 * Adds the element to the end of the container
	 * @param string - element to add
	 */
	public void add(String string) {
		String[] mas = new String[size+1];
		for (int i = 0; i < size; i++)
			mas[i] = stringarray[i];
		mas[size] = string;
		size++;
		stringarray = mas;
	}
	
	/**
	 * Removes all items from the container
	 */
	void clear() {
		stringarray = new String[0];
	}
	
	/**
	 * Removes the first case of the specified element from the container
	 * @param string - element to delete
	 * @return  true - if string deleted | false - no such element
	 */
	boolean remove(String string) {
		boolean bool = false;
		int position = -1;
		for (int i = 0; i < size; i++) {
			if (stringarray[i].equals(string)) {
				bool = true;
				position = i;
				break;
			}
		}
		if (position != -1) {			
			String[] mas = new String[size - 1];
			for (int i = 0; i < position; i++) {
				mas[i] = stringarray[i];
			}
			for (int i = position; i < size - 1; i++) {
				mas[i] = stringarray[i+1];
			}
			size--;
			stringarray = mas;
		}
		return bool;
	}
	
	/**
	 * Converting an array of strings to an array of objects
	 * @return array containing all the elements in the container
	 */
	public Object[] toArray() {
		Object[] objects = new Object[size];
		for (int i = 0; i < size; i++)
			objects[i] = stringarray[i];
		return objects;
	}
	
	/**
	 * Gets the size of container
	 * @return the number of elements
	 */
	int size() {
		return size;
	}
	
	/**
	 * Checks for the presence of an element in a container
	 * @param string element needed to find
	 * @return true - element is in the container | false - no such element
	 */
	boolean contains(String string) {
		for (int i = 0; i < size; i++)
			if (stringarray[i].equals(string))
				return true;
		return false;
	}
	
	/**
	 * Checks for the presence of all of elements in a container
	 * @param container container with elements needed to find
	 * @return true - all elements are in the container | false - no such elements in container
	 */
	boolean containsAll(Container container) {
		int count = 0;
		if (size < container.size() || container.size() == 0)
			return false;
		for (int i = 0; i < size; i++ )
			for (int j = 0; j < container.size(); j++) 
				if (stringarray[i].equals(container.stringarray[j])) {
					count++;
					break;
				}
		if (count == container.size())
			return true;
		else
			return false;
	}
	
	
	
	public Iterator<String> iterator() {
		return new MyIterator();
	}
	
	public class MyIterator implements Iterator<String>{
		int index;
		@Override
		public boolean hasNext() {
			if (index < size)
				return true;
			else
				return false;
		}
		@Override
		public String next() {
			return stringarray[index++];
		}
		@Override
		public void remove() {
			Container.this.remove(stringarray[index - 1]);
		}
	}
}
