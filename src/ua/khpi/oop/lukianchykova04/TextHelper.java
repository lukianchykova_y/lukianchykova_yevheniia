package ua.khpi.oop.lukianchykova04;

class TextHelper {
	/**
	 * Find words that start and end with the same letter 
	 * @param str The text
	 * @param dbg for debug
	 * */
	static void findWords(String str, boolean dbg) {
		StringBuilder sentence = new StringBuilder();
		char[] text = str.toCharArray();
		for (int i = 0; i < text.length; i++) {
			if ((text[i] == '.') || (text[i] == '!') || (text[i] == '?')) {
				sentence.append(text[i]);
				if(dbg) {
					System.out.println("-----------------------------------");
					System.out.println("Index of the end of sentence: " + i);
					System.out.println(sentence.toString());
					System.out.println(checkWords(sentence.toString(), dbg));
				} 
				else {
					TextInOut.printAll(sentence.toString(), checkWords(sentence.toString(), dbg));
				}
				sentence.delete(0, sentence.capacity());
					i++;	
			}
			else {
				sentence.append(text[i]);
				
			}
		}
	}
	
	/**
	 * Separetes and check words
	 * @param sentence The sentance
	 * @param dbg for debug
	 * @return sb words
	 * */
	static String checkWords(String str, boolean dbg) {
		StringBuilder checkWords = new StringBuilder();
		StringBuilder word = new StringBuilder();
		char[] sentence = str.toCharArray();
		for (int i = 0; i < sentence.length; i++) {
			if (sentence[i] == ',') {
				i++;
			}
			if ((sentence[i] == ' ') || (sentence[i] == '.') || (sentence[i] == '!') || (sentence[i] == '?')) {
				if(dbg) {
					System.out.println("Index of the end of word: " + i);
					System.out.println(word.toString());
				}
				
				checkWords = checkOneWord(word.toString(), checkWords, dbg);
				word.delete(0, word.length());
			}
			else {
				word.append(sentence[i]);
			}
		}
		return checkWords.toString();
	}
	
	/**
	 * Check word
	 * @param str The word
	 * @param sb words
	 * @param dbg for debug
	 * @return sb words
	 * */
	static StringBuilder checkOneWord(String str, StringBuilder sb, boolean dbg) {
		char[] word = str.toCharArray();
		if (word[0] == word[word.length - 1] && word.length > 1) {
			sb.append(str + "; ");
		}
		if (dbg) {
			System.out.println("Content of StringBuilder: " + sb.toString() + '\n');
		}
		return sb;
	}
	
}
