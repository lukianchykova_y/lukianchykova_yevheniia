package ua.khpi.oop.lukianchykova04;

class TextInOut {
	static String format = "|%1$-110s|%2$-50s|\n";
	/**
	 * Get the text from console
	 * @return sb The text
	 * */
	static String getText(String str) {
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		System.out.println("The text is entered.");
		return sb.toString();
	}
	
	/**
	 * Displays the table header
	 * */
	static void printFirst() {
	//	String format = "|%1$-110s|%2$-50s|\n";
			System.out.format(format, "SENTENCE", "WORDS");
			for (int i = 0; i < 163; i++)
				System.out.format("-");
			System.out.format("\n");
	}
	
	/**
	 * Displays the result in the table
	 * @param sentence The sentence
	 * @param words Result words
	 * */
	static void printAll(String sentence, String words) {
	//	String format = "|%1$-110s|%2$-50s|\n";
			System.out.format(format, sentence, words);
		for (int i = 0; i < 163; i++)
			System.out.format("-");
		System.out.format("\n");
		
	}

	
}
