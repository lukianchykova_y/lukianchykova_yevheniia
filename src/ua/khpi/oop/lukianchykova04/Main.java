package ua.khpi.oop.lukianchykova04;

import java.util.Scanner;

public class Main {
	/**
	 * Main function
	 * @param args arguments
	 * */
	public static void main(String[] args) {
		boolean dbg = false;
		for (String str : args) {
			if(str.equals("-h") || str.equals("-help")) {
				System.out.println("Autor: Lukianchykova Evheniia.");
				System.out.println("Task: Divide the text into sentences. For each sentence, print words in which the first and last letters match.\n");
				System.out.println("You can use this program in number of ways:");
				System.out.println("1) Use parameter [-d] or [-debug] to display additional data that facilitates the debugging and verification of the program.");
				System.out.println("2) Choose '1' to enter your data for further work with it.");
				System.out.println("3) Choose '2' to see entered data.");
				System.out.println("4) Choose '3' to see all words that start and end with one letter.");
				System.out.println("5) Choose '4' for exit.\n");
				
			}
			if(str.equals("-d") || str.equals("-debug")) {
				dbg = true;
			}
		}
		Menu(dbg);
	}
	
	/**
	 * Menu realisation
	 * @param dbg for debug
	 * */
	public static void Menu(boolean dbg) {
		String text = null;
		boolean theEnd = true;
		Scanner in = new Scanner(System.in);
		int menu;
		while(theEnd) {
			System.out.println("___Menu___");
			System.out.println("1. Input text");
			System.out.println("2. Show text");
			System.out.println("3. Completion of the task");
			System.out.println("4. Exit");
			System.out.print("Your choice: ");
			menu = in.nextInt();
			switch(menu) {
				case 1:
					System.out.println("Enter your text: ");
					String str = in.nextLine();
					str = in.nextLine();
					text = TextInOut.getText(str);
					break;
				case 2:
					if(text == null) {
						System.out.println("Text not found");
					}
					else {
						System.out.print("Your text: ");
						System.out.println(text);
					}
					break;
				case 3:
					if(text == null) {
						System.out.println("Text not found");
					} 
					else {
						if (dbg == false)
							TextInOut.printFirst();
						TextHelper.findWords(text, dbg);
					}
					break;
				case 4:
					theEnd = false;
					in.close();
					break;
				default: 
					System.out.println("Enter another number.");
					break;
			}
		}
	
	}

}
