package ua.khpi.oop.lukianchykova07;

import java.io.Serializable;

public class Composition implements Serializable {
	private static final long serialVersionUID = -7602632319255618013L;
	
	private int num;
	private String name;
	private String genre;
	private String performer;
	private String text;
	private int creationDay;
	private int creationMonth;
	private int creationYear;
	private String duration;
	private String dataFormat;
	private Rating[] rating;
	
	public Composition() {
		super();
	}
	
	/**
	 * Constructor
	 * @param num number of composition
	 * @param name the name of composition
	 * @param genre the genre of composition
	 * @param performer the performer of composition
	 * @param text the text of composition
	 * @param creationDay the day of creation
	 * @param creationMonth the month of creation
	 * @param creationYear the year of creation
	 * @param duration the duration of composition
	 * @param dataFormat the formate of data
	 * @param rating the rating
	 */
	public Composition(int num, String name, String genre, String performer, String text, int creationDay, int creationMonth, int creationYear, String duration, String dataFormat, Rating[] rating) {
		this.num = num;
		this.name = name;
		this.genre = genre;
		this.performer = performer;
		this.text = text;
		this.creationDay = creationDay;
		this.creationMonth = creationMonth;
		this.creationYear = creationYear;
		this.duration = duration;
		this.dataFormat = dataFormat;
		this.rating = rating;
	}
	
	/**
	 * Getter of number
	 * @return number
	 */
	public int getNum() {
		return num;
	}
	
	/**
	 * Setter of number
	 * @param num number
	 */
	public void setNum(int num) {
		this.num = num;
	}
	
	/**
	 * Getter of name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter of name
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter of genre
	 * @return genre
	 */
	public String getGenre() {
		return genre;
	}
	
	/**
	 * Setter of genre
	 * @param genre genre
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	/**
	 * Getter of performer
	 * @return performer
	 */
	public String getPerformer() {
		return performer;
	}
	
	/**
	 * Setter of performer
	 * @param performer performer
	 */
	public void setPerformer(String performer) {
		this.performer = performer;
	}
	
	/**
	 * Getter of text
	 * @return text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Setter of text
	 * @param text text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Getter of creation day
	 * @return creation day
	 */
	public int getCreationDay() {
		return creationDay;
	}
	
	/**
	 * Setter of creation day
	 * @param creationDay day of creation 
	 */
	public void setCreationDay(int creationDay) {
		this.creationDay = creationDay;
	}
	
	/**
	 * Getter of creation month
	 * @return creation month
	 */
	public int getCreationMonth() {
		return creationMonth;
	}
	
	/**
	 * Setter of creation month
	 * @param creationMonth month of creation 
	 */
	public void setCreationMonth(int creationMonth) {
		this.creationMonth = creationMonth;
	}
	
	/**
	 * Getter of creation year
	 * @return creation year
	 */
	public int getCreationYear() {
		return creationYear;
	}
	
	/**
	 * Setter of creation year
	 * @param creationYear year of creation 
	 */
	public void setCreationYear(int creationYear) {
		this.creationYear = creationYear;
	}
	
	/**
	 * Getter of duration
	 * @return duration
	 */
	public String getDuration() {
		return duration;
	}
	
	/**
	 * Setter of duration
	 * @param duration duration
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	/**
	 * Getter of data format
	 * @return data formate
	 */
	public String getDataFormat() {
		return dataFormat;
	}
	
	/**
	 * Setter of data format
	 * @param dataFormat data formate
	 */
	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}
	
	/**
	 * Getter of rating
	 * @return rating
	 */
	public Rating[] getRating() {
		return rating;
	}
	
	/**
	 * Setter of rating
	 * @return rating rating
	 */
	public void setRating(Rating[] rating) {
		this.rating = rating;
	}
	
	/**
	 * Print composition
	 */
	public void print() { 
		System.out.println(num + ". " + name + " -- " + performer);
		System.out.println("Жанр: " + genre);
		System.out.println("Длительность: " + duration);
		System.out.println("Дата выхода песни: " + creationDay + "/" + creationMonth + "/" + creationYear);
		System.out.println("Формат: " + dataFormat);
		System.out.println("----- Текст -----");
		if (text == null) {
			System.out.println("Текст песни недоступен");
		} else {
			System.out.println(text);
		}
		printRating();
		System.out.println("--------------------");
		System.out.println("\n");
	}
	
	public void printRating() {
		System.out.println("----- Рейтинг -----");
		for (int i = 0 ; i < rating.length; i++) {
			System.out.println(rating[i].getProperty() + ": " + rating[i].getValue());
		}
	}

}
