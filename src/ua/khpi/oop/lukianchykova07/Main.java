package ua.khpi.oop.lukianchykova07;

public class Main {
	
	/**
	 * Main function
	 * @param args arguments
	 * */
	public static void main(String[] args) {
		AudioLibrary list = new AudioLibrary();
		String text = "Позови меня с собой,\nЯ приду сквозь злые ночи,\nЯ отправлюсь за тобой,\nЧтобы путь мне не пророчил,\nЯ приду туда, где ты\nНарисуешь в небе солнце,\nГде разбитые мечты\nОбретают снова силу высоты";
		Rating[] rating = new Rating[3];
		list.rate[0] = new Rating("Прослушали", 1940400);
		list.rate[1] = new Rating("Продажи", 10000);
		list.rate[2] = new Rating("Радио", 2500);
		list.mas[0] = new Composition(1, "Позови меня с собой", "Поп-музыка", "Алла Пугачёва", text, 21, 8, 1995, "4:17", ".mp3", list.rate);
		text = null;
		rating[0] = new Rating("Прослушали", 1000000);
		rating[1] = new Rating("Продажи", 4000);
		rating[2] = new Rating("Радио", 500);
		list.mas[1] = new Composition(2, "Каждый кто делал тебе больно", "Авторская песня", "Леро4ка", text, 9, 7, 2021, "3:33", ".mp3", rating);
		text = "На ветру белое платье,\nНа тебе проклятье,\nМне нужны объятья, поцелуи,\nЧасть тебя.\nРазлетелся пепел в поле,\nЯ бегу по полю,\nС тобою небеса.";
		rating[0] = new Rating("Прослушали", 774000);
		rating[1] = new Rating("Продажи", 1000);
		rating[2] = new Rating("Радио", 200);
		list.mas[2] = new Composition(3, "Ведьмы", "Авторская песня", "BaVkon", text, 1, 12, 2021, "1:34", ".mp3", rating);
		list.printAll();
	}

}
