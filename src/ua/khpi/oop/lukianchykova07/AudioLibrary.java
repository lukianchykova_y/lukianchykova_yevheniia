package ua.khpi.oop.lukianchykova07;

public class AudioLibrary {
	Composition[] mas = new Composition[3];
	Rating[] rate = new Rating[3];
	
	/**
	 * Prints all compositions
	 */
	public void printAll() { 
		for(int i = 0 ; i < mas.length; i++) {
			System.out.println(mas[i].getNum() + ". " + mas[i].getName() + " -- " + mas[i].getPerformer());
			System.out.println("Жанр: " + mas[i].getGenre());
			System.out.println("Длительность: " + mas[i].getDuration());
			System.out.println("Дата выхода песни: " + mas[i].getCreationDay() + "/" + mas[i].getCreationMonth() + "/" + mas[i].getCreationYear());
			System.out.println("Формат: " + mas[i].getDataFormat());
			System.out.println("----- Текст -----");
			if (mas[i].getText() == null) {
				System.out.println("Текст песни недоступен");
			} else {
				System.out.println(mas[i].getText());
			}
			mas[i].printRating();
			System.out.println("--------------------");
			System.out.println("\n");
		}
	}
}
