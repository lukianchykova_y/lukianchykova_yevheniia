package ua.khpi.oop.lukianchykova07;

import java.io.Serializable;

public class Rating implements Serializable {
	private static final long serialVersionUID = -1901414634900826783L;

	private String property;
	private int value;
	
	/**
	 * Constructor
	 * @param property the property
	 * @param value value of the property
	 */
	public Rating(String property, int value) {
		this.property = property;
		this.value = value;
	}
	
	public Rating() {
		super();	
	}
	
	/**
	 * Getter of property
	 * @return property
	 */
	public String getProperty() {
		return property;
	}
	
	/**
	 * Setter of property
	 * @param property property
	 */
	public void setProperty(String property) {
		this.property = property;
	}
	
	/**
	 * Getter of value
	 * @return value
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * Setter of value
	 * @param value value
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
}
