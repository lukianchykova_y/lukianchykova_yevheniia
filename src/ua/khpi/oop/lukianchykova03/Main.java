package ua.khpi.oop.lukianchykova03;

public class Main {
	/**
	 * Main function
	 * @param args arguments of function
	 * */
	public static void main(String[] args) {
		String text = SameFirstLast.getText();
		SameFirstLast.findWords(text);
	}

}
