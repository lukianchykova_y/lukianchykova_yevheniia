package ua.khpi.oop.lukianchykova03;

import java.util.Scanner;

public class SameFirstLast {
	/**
	 * Find words that start and end with the same letter 
	 * @param str The text
	 * */
	public static void findWords(String str) {
		StringBuilder sentence = new StringBuilder();
		char[] text = str.toCharArray();
		printFirst();
		for (int i = 0; i < text.length; i++) {
			if ((text[i] == '.') || (text[i] == '!') || (text[i] == '?')) {
				sentence.append(text[i]);
				printAll(sentence.toString(), checkWords(sentence.toString()));
				sentence.delete(0, sentence.capacity());
					i++;	
			}
			else {
				sentence.append(text[i]);
				
			}
		}
		
	}
	
	/**
	 * Separetes and check words
	 * @param sentence The sentance
	 * @return sb words
	 * */
	public static String checkWords(String str) {
		StringBuilder checkWords = new StringBuilder();
		StringBuilder word = new StringBuilder();
		char[] sentence = str.toCharArray();
		for (int i = 0; i < sentence.length; i++) {
			if (sentence[i] == ',') {
				i++;
			}
			if ((sentence[i] == ' ') || (sentence[i] == '.') || (sentence[i] == '!') || (sentence[i] == '?')) {
				checkWords = checkOneWord(word.toString(), checkWords);
				word.delete(0, word.length());
			}
			else {
				word.append(sentence[i]);
			}
		}
		return checkWords.toString();
	}
	
	/**
	 * Check word
	 * @param str The word
	 * @return sb words
	 * */
	private static StringBuilder checkOneWord(String str, StringBuilder sb) {
		char[] word = str.toCharArray();
		if(word[0] == word[word.length - 1] && word.length > 1) {
			sb.append(str + "; ");
		}
		return sb;
	}
	
	/**
	 * Get the text from console
	 * @return sb The text
	 * */
	public static String getText() {
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(System.in);
		sb.append(s.nextLine());
		s.close();
		return sb.toString();
	}
	
	/**
	 * Displays the table header
	 * */
	private static void printFirst() {
		String format = "|%1$-110s|%2$-50s|\n";
			System.out.format(format, "SENTENCE", "WORDS");
			for (int i = 0; i < 163; i++)
				System.out.format("-");
			System.out.format("\n");
	}
	
	/**
	 * Displays the result in the table
	 * @param sentence The sentence
	 * @param words Result words
	 * */
	private static void printAll(String sentence, String words) {
		String format = "|%1$-110s|%2$-50s|\n";
			System.out.format(format, sentence, words);
		for (int i = 0; i < 163; i++)
			System.out.format("-");
		System.out.format("\n");
		
	}
}
