package ua.khpi.oop.lukianchykova03;

import java.util.Scanner;

class TextInOut {
	/**
	 * Get the text from console
	 * @return sb The text
	 * */
	static String getText() {
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(System.in);
		sb.append(s.nextLine());
		s.close();
		return sb.toString();
	}
	
	/**
	 * Displays the table header
	 * */
	static void printFirst() {
		String format = "|%1$-110s|%2$-50s|\n";
			System.out.format(format, "SENTENCE", "WORDS");
			for (int i = 0; i < 163; i++)
				System.out.format("-");
			System.out.format("\n");
	}
	
	/**
	 * Displays the result in the table
	 * @param sentence The sentence
	 * @param words Result words
	 * */
	static void printAll(String sentence, String words) {
		String format = "|%1$-110s|%2$-50s|\n";
			System.out.format(format, sentence, words);
		for (int i = 0; i < 163; i++)
			System.out.format("-");
		System.out.format("\n");
		
	}
	
	
}
