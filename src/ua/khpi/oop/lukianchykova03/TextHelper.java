package ua.khpi.oop.lukianchykova03;

class TextHelper {
	/**
	 * Find words that start and end with the same letter 
	 * @param str The text
	 * */
	static void findWords(String str) {
		StringBuilder sentence = new StringBuilder();
		char[] text = str.toCharArray();
		for (int i = 0; i < text.length; i++) {
			if ((text[i] == '.') || (text[i] == '!') || (text[i] == '?')) {
				sentence.append(text[i]);
				TextInOut.printAll(sentence.toString(), checkWords(sentence.toString()));
				sentence.delete(0, sentence.capacity());
					i++;	
			}
			else {
				sentence.append(text[i]);
				
			}
		}
		
	}
	
	/**
	 * Separetes and check words
	 * @param sentence The sentance
	 * @return sb words
	 * */
	static String checkWords(String str) {
		StringBuilder checkWords = new StringBuilder();
		StringBuilder word = new StringBuilder();
		char[] sentence = str.toCharArray();
		for (int i = 0; i < sentence.length; i++) {
			if (sentence[i] == ',') {
				i++;
			}
			if ((sentence[i] == ' ') || (sentence[i] == '.') || (sentence[i] == '!') || (sentence[i] == '?')) {
				checkWords = checkOneWord(word.toString(), checkWords);
				word.delete(0, word.length());
			}
			else {
				word.append(sentence[i]);
			}
		}
		return checkWords.toString();
	}
	
	/**
	 * Check word
	 * @param str The word
	 * @return sb words
	 * */
	static StringBuilder checkOneWord(String str, StringBuilder sb) {
		char[] word = str.toCharArray();
		if(word[0] == word[word.length - 1] && word.length > 1) {
			sb.append(str + "; ");
		}
		return sb;
	}
}
