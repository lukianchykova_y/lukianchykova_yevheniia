package ua.khpi.oop.lukianchykova10;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import ua.khpi.oop.lukianchykova07.Composition;
import ua.khpi.oop.lukianchykova07.Rating;

public class Main {

	public static void main(String[] args) {
		MyContainer<Composition> audioLibrary = new MyContainer<Composition>();
		
		for (String str : args) {
			if(str.equals("-a") || str.equals("-auto")) {
				audioLibrary = auto(audioLibrary);
				return;
			}
		}
		
		Menu(audioLibrary);
	}
	
	private static MyContainer<Composition> auto(MyContainer<Composition> audioLibrary) {
		System.out.println("Adding elements...");
		String filenameDeserialization = "audioLibrary10.xml";
		try(XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
			audioLibrary.clear();
			audioLibrary = (MyContainer<Composition>) decoder.readObject();
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
		
		System.out.println("Adding was end.\n");
		
		System.out.println("List in Audio library:\n");
		if(audioLibrary.getSize() > 0) {
			for(var element : audioLibrary) {
				element.print();
			}
		}
		else {
			System.out.println("The list is empty!\n");
		}
		
		int orderSort = 1;
		
		audioLibrary.sort(new avgRatingComparator(), orderSort);
		System.out.println("Data sorted by rating");
		
		System.out.println("List in Audio library:\n");
		if(audioLibrary.getSize() > 0) {
			for(var element : audioLibrary) {
				element.print();
			}
		}	
		return audioLibrary;
	}
	
	/**
	 * Menu realisation
	 * @param 
	 * */
	public static void Menu(MyContainer<Composition> audioLibrary) {
		boolean endprog = false;
		Scanner inInt = new Scanner(System.in);
		Scanner inStr = new Scanner(System.in);
		int menu;
		int menuSort;
		int orderSort;
		
		while(!endprog) {
			System.out.println("1. Show all compositions");
			System.out.println("2. Add composition");
			System.out.println("3. Delete composition");
			System.out.println("4. Clear list");
			System.out.println("5. Check for emptiness");
			System.out.println("6. Sort data");
			System.out.println("7. Serialize data");
			System.out.println("8. Deserialize data");
			System.out.println("0. Exit");
			System.out.print("Enter option: ");
			try {
				menu =  inInt.nextInt();
			}
			catch(java.util.InputMismatchException e) {
				System.out.println("Error! Ошибка ввода.");
				endprog = true;
				menu = 0;
			}
			System.out.println();
			
			switch(menu) {
			case 1:
				if(audioLibrary.getSize() > 0) {
					for(var element : audioLibrary) {
						element.print();
					}
				}
				else {
					System.out.println("The list is empty!\n");
				}
				break;
			case 2:
				if(addComposition(audioLibrary, inInt, inStr) == -1) {
					break;
				}
				break;
			case 3:
				System.out.println("Enter number of composition to delete: ");
				int delete = inInt.nextInt();
				boolean isExist = false;
				if(audioLibrary.getSize() > 0) {
					for(Composition element : audioLibrary) {
						if(element.getNum() == delete) {
							isExist = true;
						}
						if(element.getNum() > delete) {
							element.setNum(element.getNum() - 1);
						}
					}
					if(isExist) {
						if(audioLibrary.delete(delete - 1)) {
							System.out.println("Composition was deleted successfully.");
						} else
							System.out.println("Error! Wrong number.");
					} else
						System.out.println("Error! Wrong number.");
				}
				break;
			case 4:
				audioLibrary.clear();
				System.out.println("The list is empty now.\n");
				break;
			case 5:
				if(audioLibrary.isEmpty())
					System.out.println("The list is empty.\n");
				else
					System.out.println("The list is not empty.");
				break;
			case 6:
				System.out.println("1. Sort by name of composition");
				System.out.println("2. Sort by performer");
				System.out.println("3. Sort by average rating");
				System.out.println("0. Return to menu");
				System.out.println("Enter option: ");
				try {
					menuSort =  inInt.nextInt();
				}
				catch(java.util.InputMismatchException e) {
					System.out.println("Error! Ошибка ввода.");
					break;
				}
				System.out.println();
				System.out.println("How to sort data?");
				System.out.println("1. Asc");
				System.out.println("2. Desc");
				System.out.println("Enter option: ");
				try {
					orderSort =  inInt.nextInt();
				}
				catch(java.util.InputMismatchException e) {
					System.out.println("Error! Ошибка ввода.");
					break;
				}
				switch(menuSort) {
				case 1:
					audioLibrary.sort(new nameComparator(), orderSort);
					System.out.println("Data sorted by name of composition\n");
					break;
				case 2:
					audioLibrary.sort(new performerComparator(), orderSort);
					System.out.println("Data sorted by performer\n");
					break;
				case 3:
					audioLibrary.sort(new avgRatingComparator(), orderSort);
					System.out.println("Data sorted by rating\n1");
					break;
				case 0:
		
					break;
				default:
					System.out.println("Error! Wrong num in Sort menu.");
					break;
				}
				break;
			case 7:
				String filenameSerialization;
				String filenameXML;
				int menuSerialization;
				
				System.out.println("1. Serialization");
				System.out.println("2. XML serialization");
				System.out.println("0. Exit serialization");
				try 
				{
					menuSerialization =  inInt.nextInt();
				}
				catch(java.util.InputMismatchException e) 
				{
					System.out.println("Error! Ошибка ввода.");
					menuSerialization = 0;
				}
				switch(menuSerialization) 
				{
				case 1:
					System.out.println("\nEnter file name: ");
					filenameSerialization = inStr.nextLine();
					if (filenameSerialization.indexOf(".ser") == -1) {
						filenameSerialization += ".ser";
					}
					try(ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream (filenameSerialization)))){
						oos.writeObject(audioLibrary);
						System.out.println("Serialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 2:
					System.out.print("Enter XML filename: ");
					filenameXML = inStr.nextLine();
					if (filenameXML.indexOf(".xml") == -1)
						filenameXML += ".xml";
					try(XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream (filenameXML)))){
						encoder.writeObject(audioLibrary);
						System.out.println("Serialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 0:
					break;
				default:
					System.out.println("Error! Wrong num in menu.");
					break;
				}
				break;
			case 8:
				String filenameDeserialization;
				int menuDeserialization;
				
				System.out.println("1. Deserialization");
				System.out.println("2. XML deserialization");
				System.out.println("0. Exit deserialization");
				try 
				{
					menuDeserialization =  inInt.nextInt();
				}
				catch(java.util.InputMismatchException e) 
				{
					System.out.println("Error! Ошибка ввода.");
					menuDeserialization = 0;
				}
				switch(menuDeserialization) 
				{
				case 1:
					System.out.println("\nEnter file name: ");
					filenameDeserialization = inStr.nextLine();
					if (filenameDeserialization.indexOf(".ser") == -1) {
						filenameDeserialization += ".ser";
					}
					try(ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
						audioLibrary.clear();
						audioLibrary=(MyContainer<Composition>) ois.readObject();
						System.out.println("Deserialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 2:
					System.out.print("Enter XML filename: ");
					filenameDeserialization = inStr.nextLine();
					if (filenameDeserialization.indexOf(".xml") == -1)
						filenameDeserialization += ".xml";
					try(XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
						audioLibrary.clear();
						audioLibrary =  (MyContainer<Composition>) decoder.readObject();
						System.out.println("Deserialization successful.");
					} catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 0:
					break;
				default:
					System.out.println("Error! Wrong num in menu.");
					break;
				}
				break;
			case 0:
				endprog = true;
				inInt.close();
				inStr.close();
				break;
			default:
				System.out.println("Error! Wrong num.");
				break;
			}
		}
	}
	
	/**
	 * Add new composition
	 * @param 
	 * */
	public static int addComposition(MyContainer<Composition> audioLibrary, Scanner inInt, Scanner inStr) {
		int num = audioLibrary.getSize() + 1;
		Rating[] rate = new Rating[3];
		String text = null;
		String name;
		String performer;
		String genre;
		int day;
		int month;
		int year;
		String duration;
		String format;
		
		System.out.println("Введите название: ");
		try {
			name = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите исполнителя: ");
		try {
			performer = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите жанр: ");
		try {
			genre = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Есть ли текст? [Да(1)/Нет(0)]: ");
		int c = inInt.nextInt();
		if (c == 1) {
			System.out.println("Введите текст: ");
			try {
				text = inStr.nextLine();
			}catch(java.util.InputMismatchException e) {
				System.out.println("Error! Incorect input!");
				return -1;
			}
		}
		
		System.out.println("Введите день выпуска: ");
		try {
			day = inInt.nextInt();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите месяц выпуска: ");
		try {
			month = inInt.nextInt();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите год выпуска: ");
		try {
			year = inInt.nextInt();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите длительность: ");
		try {
			duration = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите формат: ");
		try {
			format = inStr.nextLine();
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		System.out.println("Введите кол-во прослушиваний: ");
		try {
			rate[0] = new Rating("Прослушали", inInt.nextInt());
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		System.out.println("Введите кол-во продаж: ");
		try {
			rate[1] = new Rating("Продажи", inInt.nextInt());
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		System.out.println("Введите кол-во прокруток по радио: ");
		try {
			rate[2] = new Rating("Радио", inInt.nextInt());
		}catch(java.util.InputMismatchException e) {
			System.out.println("Error! Incorect input!");
			return -1;
		}
		
		Composition composition = new Composition(num, name, genre, performer, text, day, month, year, duration, format, rate);
		audioLibrary.add(composition);
		return 1;
	}
	
}