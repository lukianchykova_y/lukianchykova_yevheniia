package ua.khpi.oop.lukianchykova10;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import ua.khpi.oop.lukianchykova07.*;


public class MyContainer<T> implements Iterable<T>, Serializable {
	private static final long serialVersionUID = 707932790294563395L;
	
	public Node<T> head;
	private int size;
	
	public MyContainer() {
		super();
	}
	
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public T getElement(int id) {
		if(id < 0 || id > size) {
			System.out.println("Error! Wrong ID.");
			return null;
		}
		Node<T> temp = head;
		for(int i = 0; id > i; i++)	{
			temp = temp.next;
		}
		return temp.element;
	}
	
	public void add(T element) {
		Node<T> tmp = new Node<T>();
		
		if(head == null) {
			head = new Node<T>(element);
		} else {
			tmp = head;
			while(tmp.next != null) {
				tmp = tmp.next;
			}
			tmp.next = new Node<T>(element);
		}
		size++;
	}
	
	public boolean delete(int id) {
		Node<T> tmp = head;
	
		if(head != null) {
			if(id == 0) {
				head = head.next;
				size--;
			} else {
				for(int i = 0; id-1 > i; i++)
					tmp= tmp.next;
				
				if(tmp.next.next != null) {
					tmp.next = tmp.next.next;
				}
				else
					tmp.next = null;
				size--;
			}
			return true;
		} else {
			System.out.println("Container is empty!");
			return false;
		}
	}
	
	public void clear() {
		head = null;
		size = 0;
	}
	
	public Object[] toArray() {		
		Object[] array = new Object[size];
		for(int i = 0; size > i; i++) {
			array[i] = getElement(i);
		}
		return array;
	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		for(T element : this) {
			str.append(element + "\n");
		}
		return str.toString();
	}
	
	public boolean isEmpty() {
		if(size == 0)
			return true;
		else
			return false;
	}
	
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			int index = 0;
			boolean check = false;
			
			@Override 
			public boolean hasNext() {
				return size > index;
			}
			
			@Override
			public T next() {
				if(index != size) {
					check = true;
					return getElement(index++);
				}
				else
					throw new NoSuchElementException();
			}
			
			@Override
			public void remove() {
				if(check) {
					MyContainer.this.delete(index - 1);
					check = false;
				}
			}
			
		};
	}
		
	public void sort (Comparator<T> comp, int order) {
		Object[] array = this.toArray();
		Object temp;
		boolean check;
			
		if (order == 1) {
			do {
				check = false;
				for(int i = 0; size - 1 > i; i++) {
					if(comp.compare((T)array[i],(T)array[i+1]) == 1) {
						temp = array[i];
						array[i] = array[i + 1];
						array[i + 1] = temp;
						check = true;
					}
				}
			} while (check == true);
		}
		else {
			do {
				check = false;
				for(int i = 0; size - 1 > i; i++) {
					if(comp.compare((T)array[i],(T)array[i+1]) == -1) {
						temp = array[i+1];
						array[i+1] = array[i];
						array[i] = temp;
						check = true;
					}
				}
			} while (check == true);
		}
				
		this.clear();
		for(Object obj : array) {
			this.add((T)obj);
		}
	}
}

class nameComparator implements Comparator<Composition>{
	@Override
	public int compare(Composition o1, Composition o2) { 
		if(o1.getName().toCharArray()[0] > o2.getName().toCharArray()[0])
			return 1;
		else if (o1.getName().toCharArray()[0] < o2.getName().toCharArray()[0])
			return -1;
		else
			return 0;
	}
}

class performerComparator implements Comparator<Composition>{
	@Override
	public int compare(Composition o1, Composition o2) {
		if(o1.getPerformer().toCharArray()[0] > o2.getPerformer().toCharArray()[0])
			return 1;
		else if (o1.getPerformer().toCharArray()[0] < o2.getPerformer().toCharArray()[0])
			return -1;
		else
			return 0;
	}
}

class avgRatingComparator implements Comparator<Composition>{
	@Override
	public int compare(Composition o1, Composition o2) {
		int firstAvg = 0;
		int secondAvg = 0;
		for (int i = 0; i < o1.getRating().length; i++) {
			firstAvg += o1.getRating()[i].getValue();
		}
		for (int i = 0; i < o2.getRating().length; i++) {
			secondAvg += o2.getRating()[i].getValue();
		}
		if(firstAvg > secondAvg)
			return 1;
		else if (firstAvg < secondAvg)
			return -1;
		else
			return 0;
	}
} 
