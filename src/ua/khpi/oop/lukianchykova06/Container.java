package ua.khpi.oop.lukianchykova06;

import java.io.Serializable;
import java.util.Iterator;

public class Container implements Serializable {
	private static final long serialVersionUID = 7079352123023118854L;
	private String[] stringArray;
	private int size = 0;
	
	/**
	 * Constructor
	 * @param strarr - string array
	 */
	public Container (String... strarr) {
		if (strarr.length != 0) {
			size = strarr.length;
			stringArray = new String [size];
			for (int i = 0; i < size; i++)
				stringArray[i] = strarr[i];
		}
	}
	
	/**
	 * Returns yhe contents of the container as a string
	 * @return sb contents as string
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++)
			sb.append(stringArray[i] + " ");
		return sb.toString();
	}
	
	/**
	 * Adds the element to the end of the container
	 * @param string - element to add
	 */
	public void add(String string) {
		String[] mas = new String[size+1];
		for (int i = 0; i < size; i++)
			mas[i] = stringArray[i];
		mas[size] = string;
		size++;
		stringArray = mas;
	}
	
	/**
	 * Removes all items from the container
	 */
	void clear() {
		stringArray = new String[0];
	}
	
	/**
	 * Removes the first case of the specified element from the container
	 * @param string - element to delete
	 * @return  true - if string deleted | false - no such element
	 */
	boolean remove(String string) {
		boolean bool = false;
		int position = -1;
		for (int i = 0; i < size; i++) {
			if (stringArray[i].equals(string)) {
				bool = true;
				position = i;
				break;
			}
		}
		if (position != -1) {			
			String[] mas = new String[size - 1];
			for (int i = 0; i < position; i++) {
				mas[i] = stringArray[i];
			}
			for (int i = position; i < size - 1; i++) {
				mas[i] = stringArray[i+1];
			}
			size--;
			stringArray = mas;
		}
		return bool;
	}
	
	/**
	 * Converting an array of strings to an array of objects
	 * @return array containing all the elements in the container
	 */
	public Object[] toArray() {
		Object[] objects = new Object[size];
		for (int i = 0; i < size; i++)
			objects[i] = stringArray[i];
		return objects;
	}
	
	/**
	 * Gets the size of container
	 * @return the number of elements
	 */
	int size() {
		return size;
	}
	
	/**
	 * Checks for the presence of an element in a container
	 * @param string element needed to find
	 * @return true - element is in the container | false - no such element
	 */
	boolean contains(String string) {
		for (int i = 0; i < size; i++)
			if (stringArray[i].equals(string))
				return true;
		return false;
	}
	
	/**
	 * Checks for the presence of all of elements in a container
	 * @param container container with elements needed to find
	 * @return true - all elements are in the container | false - no such elements in container
	 */
	boolean containsAll(Container container) {
		int count = 0;
		if (size < container.size() || container.size() == 0)
			return false;
		for (int i = 0; i < size; i++ )
			for (int j = 0; j < container.size(); j++) 
				if (stringArray[i].equals(container.stringArray[j])) {
					count++;
					break;
				}
		if (count == container.size())
			return true;
		else
			return false;
	}
	
	/**
	 * Print all elements
	 */
	public void printContainer() {
		if (size() > 0) {
			for(String str : stringArray)
				System.out.println(str);
		} 
		else {
			System.out.println("Conatainer is empty.");
		}
		System.out.println();
	}
	
	/**
	 * Find the position of element in container
	 * @param string element
	 * @return position of elment / -1 - no such elementin list
	 * */
	public int find(String string) {
		int position = 0;
		for (String str : stringArray) {
			if (str.equals(string))
				return position;
			position++;
		}
		return -1;
	}
	
	
	/**
	 * Sort container
	 * @param sortType type of sorting(ascendig/descending)
	 */
	public void sortAlphabet(int sortType) {
		if (sortType == 1) {
			for (int i = 0; i < size - 1; i++)
				for (int j = 0; j < size - 1; j++)
					if (stringArray[j].compareTo(stringArray[j+1]) > 0) {
						String temp = stringArray[j];
						stringArray[j] = stringArray[j+1];
						stringArray[j+1] = temp;
					}
		}
		else if (sortType == 2) {
			for (int i = 0; i < size - 1; i++)
				for (int j = 0; j < size - 1; j++)
					if (stringArray[j].compareTo(stringArray[j+1]) < 0) {
						String temp = stringArray[j];
						stringArray[j] = stringArray[j+1];
						stringArray[j+1] = temp;
					}
		}
	}
	
	/**
	 * Compare two elements
	 * @param position1 position of first element
	 * @param position2 position of second element
	 * @return -1 - incorrect positions are specified | 1 - strings are equals | 0 - strings are not equals 
	 */
	public int compareElements (int first, int second) {
		if (first > size || second > size || first < 0 || second < 0 )
			return -1;
		if (stringArray[first - 1].equals(stringArray[second - 1]))
			return 1;
		else
			return 0;
	}
	
	public Iterator<String> iterator() {
		return new MyIterator();
	}
	
	public class MyIterator implements Iterator<String>{
		int index;
		@Override
		public boolean hasNext() {
			if (index < size)
				return true;
			else
				return false;
		}
		@Override
		public String next() {
			return stringArray[index++];
		}
		@Override
		public void remove() {
			Container.this.remove(stringArray[index - 1]);
		}
	}
}
