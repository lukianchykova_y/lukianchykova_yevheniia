package ua.khpi.oop.lukianchykova02;

import java.util.Random;

public class Main {
	/**
	 * Finds the sum of the digits of a number
	 * @param number number
	 * @return res result
	 * */
	private static int findSum(int number) {
		int res = 0;
		while(number > 0) {
			res += number % 10;
			number /= 10;
		}
 		return res;
	}
	/**
	 * Displays the table header
	 * */
	private static void printFirst() {
		System.out.println("Task. To find the sum of the digits of an integer.");
		System.out.println("+--------+--------+");
		System.out.println("| Number | Result |");
		System.out.println("+--------+--------+");
	}
	
	/**
	 * Displays the result in the table
	 * */
	private static void printRes(int num, int res) {
		System.out.format("|%8d|%8d|\n",num,res);
		System.out.println("+--------+--------+");
	}
	
	/**
	 * Finds the result for 10 random(java.util.Random) numbers
	 * */
	private static void solveTask() {
		Random rand = new Random();
		int num;
		int res;
		for(int i = 0; i < 10; i++) {
			num = rand.nextInt(999999);
			res = findSum(num);
			printRes(num, res);
		}
	}
	
	/**
	 * Main function
	 * @param args arguments of function
	 * */
	public static void main(String[] args) {
		printFirst();	
		solveTask();
	
	}
}

